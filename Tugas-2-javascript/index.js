
// A. TUGAS STRING 

// soal 1
var word = 'JavaScript';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth = 'I';
var sixth = 'love';
var seventh = 'it!';

console.log(word.concat(' ', second, ' ', third, ' ', fourth, ' ', fifth, ' ', sixth, ' ', seventh));

// soal 2

var sentence = "I am going to be React Native Developer";

var exampleFirstWord = sentence[0];
var exampleSecondWord = sentence[2] + sentence[3];
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9] + sentence[10];
var fourthWord = sentence[11] + sentence[12];
var fifthWord = sentence[14] + sentence[15];
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21] + sentence[22];
var seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var eighthWord = sentence[29] + sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log(sentence);
console.log('First Word: ' + exampleFirstWord);
console.log('Second Word: ' + exampleSecondWord);
console.log('Third Word: ' + thirdWord);
console.log('Fourth Word: ' + fourthWord);
console.log('Fifth Word: ' + fifthWord);
console.log('Sixth Word: ' + sixthWord);
console.log('Seventh Word: ' + seventhWord);
console.log('Eighth Word: ' + eighthWord)

// soal 3
var sentence2 = 'wow JavaScript is so cool';

var exampleFirstWord2 = sentence2.substring(0, 3);
var secondWord2 = sentence2.substr(4, 10);
var thirdWord2 = sentence2.substr(15, 2);
var fourthWord2 = sentence2.substr(18, 2);
var fifthWord2 = sentence2.substr(20, 5);

console.log('First Word: ' + exampleFirstWord2);
console.log('Second Word: ' + secondWord2);
console.log('Third Word: ' + thirdWord2);
console.log('Fourth Word: ' + fourthWord2);
console.log('Fifth Word: ' + fifthWord2);


// soal 4
var sentence3 = 'wow JavaScript is so cool';

var exampleFirstWord3 = sentence3.substring(0, 3);
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3 = sentence3.substring(18, 20);
var fifthWord3 = sentence3.substring(21, 25);

var firstWordLength = exampleFirstWord3.length
var secondWord3Length = secondWord3.length
var thirdWord3Length = thirdWord3.length
var fourthWord3Length = fourthWord3.length
var fifthWord3Length = fifthWord3.length
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength);
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWord3Length);
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWord3Length);
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWord3Length);
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWord3Length);


// B TUGAS CONDITIONAL

// Soal if else 
var nama = 'Junaedi'
var peran = 'Werewolf'

if (nama === '' && peran === '') {
    console.log('nama dan peran wajib diisi');
} else if (nama != '' && peran == '') {
    console.log(`Halo ${nama}, pilih peranmu untuk mulai game`);
} else if (nama != '' && peran != '') {
    if (nama == 'jane' && peran == 'Penyihir') {
        console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
        console.log(`Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`)
    } else if (nama == 'Jenita' && peran == 'Guard') {
        console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
        console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`)
    } else if (nama == 'Junaedi' && peran == 'Werewolf') {
        console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
        console.log(`Halo Werewolf ${nama}, Kamu akan memakan mangsa setiap malam!`)
    }
}

// soal switch case 

var tanggal = 25;
var bulan = 10;
var tahun = 2020;

switch (bulan) {
    case 1:
        console.log(tanggal, ' Januari', tahun)
        break;
    case 2:
        console.log(tanggal, ' Februari', tahun)
        break;
    case 3:
        console.log(tanggal, ' Maret', tahun)
        break;
    case 4:
        console.log(tanggal, ' April', tahun)
        break;
    case 5:
        console.log(tanggal, ' Mei', tahun)
        break;
    case 6:
        console.log(tanggal, ' Juni', tahun)
        break;
    case 7:
        console.log(tanggal, ' Juli', tahun)
        break;
    case 8:
        console.log(tanggal, ' Agustus', tahun)
        break;
    case 9:
        console.log(tanggal, ' September', tahun)
        break;
    case 10:
        console.log(tanggal, ' Oktober', tahun)
        break;
    case 11:
        console.log(tanggal, ' November', tahun)
        break;
    case 12:
        console.log(tanggal, ' Desember', tahun)
        break;
}
