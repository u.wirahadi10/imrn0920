// No. 1 Looping while 
console.log('LOOPING PERTAMA');
var incream = 2;
while (incream <= 20) {
    console.log(`${incream} - I love coding`);
    incream += 2;
}

console.log('LOOPING KEDUA');
var decream = 20;
while (decream > 0) {
    console.log(`${decream} - I will become a mobile developer`);
    decream -= 2;
}

//No. 2 Looping For
var kelipatan3 = 3;
for (var i = 1; i <= 20; i++) {
    if ((i % 2) == 0) {
        console.log(`${i} - berkualitas`)
    } else {
        if (i % 3 == 0) {
            console.log(`${i} - I Love Coding `)
        }
        console.log(`${i} - santai`)
    }
}

//No. 3 Membuat Persegi Panjang
console.log('no. 3-------------------------------');
var isi = '';
for (let y = 1; y <= 4; y++) {
    for (let x = 0; x < 8; x++) {
        isi += '#';
    }
    isi += '\n';
}

console.log(isi);
console.log('-------------------------------');

//No. 4 Membuat Tangga
console.log('no. 4-------------------------------');
var isitangga = '';
for (let y = 1; y <= 7; y++) {
    for (let x = 0; x < y; x++) {
        isitangga += '#';
    }
    isitangga += '\n';
}

console.log(isitangga);

//No. 5 Membuat Papan catur
console.log('no. 5-------------------------------');
var isicatur = '';
for (let y = 1; y <= 8; y++) {
    for (let x = 0; x < 8; x++) {
        if (y % 2 == 1) {
            if (x % 2 == 1) {
                isicatur += ' ';
            } else {
                isicatur += '#';
            }
        } else {
            if (x % 2 == 1) {
                isicatur += '#';
            } else {
                isicatur += ' ';
            }
        }
    }
    isicatur += '\n';
}

console.log(isicatur);
console.log('-------------------------------');