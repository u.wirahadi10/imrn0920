
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
function arrayToObject(people) {
    a = {}
    for (let i = 0; i < people.length; i++) {
        for (let j = 0; j < people[i].length; j++) {
            var sekarang = new Date();
            var thn_sekarang = sekarang.getFullYear()
            var umur = thn_sekarang - people[i][3]
            var a = ''
            if (isNaN(umur)) {
                a = 'Invalid Birth Year'
            } else {
                a = umur
            }
            a = {
                firstName: people[i][0],
                lastName: people[i][1],
                gender: people[i][2],
                age: a,
            }
        }

        console.log(i + 1, '. ', people[i][0], ' ', people[i][1], ' : ', a)
    }
}

// arrayToObject(people)

function shoppingTime(memberId1, money1) {
    uang = money1
    if (memberId1 == null || memberId1 == '') {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    }
    if (money1 <= 50000) {
        return 'Mohon maaf, uang tidak cukup'
    }

    var priceList = [1500000, 500000, 250000, 175000, 50000]
    var productName = ['Sepatu brand Stacattu', 'Baju brand Zoro', 'Baju brand H & N', 'Sweater brand Uniklooh', 'Casing Handphone ']
    var keranjang = []
    for (let i = priceList.length; i > 0; i--) {
        if (uang >= priceList[i]) {
            keranjang.push(productName[i])
        }
        // uang -= priceList[i]
    }

    var hasil = {
        memberid: memberId1,
        money: money1,
        listPurchased: keranjang
    }

    return hasil

}

// console.log(shoppingTime('CUS-001', 500000))

console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


// soal 3
function naikAngkot(a) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var penumpang = {}
    for (let k = 0; k < a.length; k++) {
        for (let l = 0; l < a[k].length; l++) {
            penumpang = {
                penumpang: a[k][0],
                naikDari: a[k][1],
                Tujuan: a[k][2],
                biaya: 0
            }
        }

    }
    return penumpang;
}

var a = [['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]
console.log(naikAngkot(a))
