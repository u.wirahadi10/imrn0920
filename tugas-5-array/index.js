// soal 1
function range(startNum, endNum) {
    var n1 = startNum;
    var n2 = endNum;
    var element = [];
    if (n1 < n2) {
        for (let i = n1; i <= n2; i++) {
            element.push(i);
        }
    } else if (n1 > n2) {
        for (let i = n1; i >= n2; i--) {
            element.push(i);
        }
    } else {
        element.unshift(-1);
    }
    return element;
}

// console.log(range());


// soal 2
function rangeWithStep(startNum, finishNum, step) {
    var n1 = startNum;
    var n2 = finishNum;
    var n3 = step;
    var element = [];
    if (n1 < n2) {
        for (let i = n1; i <= n2; i += n3) {
            element.push(i);
        }
    } else if (n1 > n2) {
        for (let i = n1; i >= n2; i -= n3) {
            element.push(i);
        }
    }

    return element;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// soal 3

function sum(startNum, finishNum, step) {
    var n1 = startNum;
    var n2 = finishNum;
    if (step == null) {
        var n3 = 1;
    } else {
        var n3 = step;
    }
    var element = [];
    if (n1 < n2) {
        for (let i = n1; i <= n2; i += n3) {
            element.push(i);
        }
    } else if (n1 > n2) {
        for (let i = n1; i >= n2; i -= n3) {
            element.push(i);
        }
    }

    var sumerize = element.reduce(function (a, b) {
        return a + b;
    }, 0)

    return sumerize;

}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0


// soal 4

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]



console.log(input[0][0])

for (let g = 0; g < input.length; g++) {
    console.log('Nomor ID : ', input[g][0])
    console.log('Nama : ', input[g][1])
    console.log('TTL : ', input[g][2], ' ', input[g][3])
    console.log('Hobi : ', input[g][4])
    console.log('=========================')

}

// soal 5
function balikKata(kata) {
    var str = kata.split('')
    var kebalik = str.reverse();
    var join = kebalik.join('')
    return join;
}


console.log(balikKata('umar wirahadi'))
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I


// soal 6


var data = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
function dataHandling2(data) {
    console.log('variable inputan awal :', data);
    data.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro");
    console.log('variable keluaran kedua  :', data);

    tanggal = data[3].split("/");
    console.log('variable keluaran ketiga  :', tanggal);
    switch (tanggal[1]) {
        case '01':
            tgl_lahir = 'Januari';
            break;
        case '02':
            tgl_lahir = 'Februari';
            break;
        case '03':
            tgl_lahir = 'Maret';
            break;
        case '04':
            tgl_lahir = 'April';
            break;
        case '05':
            tgl_lahir = 'Mei';
            break;
        case '06':
            tgl_lahir = 'Juni';
            break;
        case '07':
            tgl_lahir = 'Juli';
            break;
        case '08':
            tgl_lahir = 'Agustus';
            break;
        case '09':
            tgl_lahir = 'September';
            break;
        case '10':
            tgl_lahir = 'Oktober';
            break;
        case '11':
            tgl_lahir = 'November';
            break;
        case '12':
            tgl_lahir = 'Desember';
            break;
        default:
            tgl_lahir = 'Bulan tidak dikenal'
            break;
    }
    data[3] = tgl_lahir;
    console.log('variable keluaran keempat :', data[3])
    console.log('variable keluaran kelima :', tanggal.join('-'))
    batas = data[1].substr(0, 15)
    console.log('variable keluaran keenam :', batas)

}
dataHandling2(data);


