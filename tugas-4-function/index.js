
// soal 1
function teriak() {
    return 'Halo Sanbers!';
}
console.log(teriak());

// soal 2
function kalikan(num1, num2) {
    return num1 * num2;
}

var a = 10;
var b = 10;
var hasil = kalikan(a, b);
console.log(`${a} x ${b}= ${hasil}`);

// soal 3

function introduce(nama, umur, alamat, hobi) {
    return `Nama saya ${nama}, umur saya ${umur} tahun, alamat saya ${alamat}, dan saya punya hoby yaitu ${hobi}`
}

var name = "Umar Wirahadi"
var age = 31
var address = "Jln. Cijerah II Nusa Indah 7 blok 14 Melong CImahi selatan Kota Cimahi"
var hobby = "ngoding"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan);