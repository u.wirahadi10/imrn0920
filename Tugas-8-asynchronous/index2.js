var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

readBooksPromise(10000, books[0]).then(function (fulfilled) {
    readBooksPromise(fulfilled, books[1]).then(function (fulfilled1) {
        readBooksPromise(fulfilled1, books[2]).then(function (fulfilled2) {
            console.log(`saya memiliki waktu ${fulfilled2} detik`)
        }).catch(function (error2) {
            console.log(`saya kekurangan waktu ${error2} detik`)
        })
    }).catch(function (error1) {
        console.log(error1)
    }).catch(function (error) {
        console.log(error)
    })
})