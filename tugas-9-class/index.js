// soal 1
class animal {
    constructor(brand) {
        this.name = brand
        this.legs = 4
        this.cold_blooded = false
    }
}

var kuda = new animal("maa")

console.log(kuda.name)
console.log(kuda.legs)
console.log(kuda.cold_blooded)


// soal 2
class ape {
    constructor() {
    }

    yell() {
        console.log('Auooooo');
    }
}

var sungokong = new ape()
sungokong.yell()


class frog {
    constructor() {
    }

    jump() {
        console.log('hop hop');
    }
}

kodok = new frog()
kodok.jump()


function Clock({ template }) {

    var timer;

    function render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    this.stop = function () {
        clearInterval(timer);
    };

    this.start = function () {
        render();
        timer = setInterval(render, 1000);
    };

}

// var clock = new Clock({ template: 'h:m:s' });
// clock.start();

class clock {
    constructor(template) {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(timer);
    };

    start() {
        render();
        timer = setInterval(render, 1000);
    };
}

var jam = new clock({ template: 'h:m:s' })